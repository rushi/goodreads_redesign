# Goodreads.com Redesign

I wasn't too satisfied with the design of Goodreads.com. Some of the  issues that concern me: 

- Overuse of serifs. They're used everywhere.
- Font-sizes are a touch out of proportion
- Un-responsive layout. With so much tabular data, more area is need to display information

## Changes

I'm trying for a bit of a redesign, taking the help of Twitter's bootstrap library. I am aiming to keep the classes the same as the  original site, however writing my own CSS to override the existing
style.

Javascript will be involved, to what extent I'm not sure yet.
